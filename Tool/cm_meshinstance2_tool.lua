-- **************************************************
-- Provide Moho with the name of this script object
-- **************************************************

ScriptName = "CM_Meshinstance2_tool"

-- **************************************************
-- General information about this script
-- **************************************************

CM_Meshinstance2_tool = {}

function CM_Meshinstance2_tool:Name()
	return 'Meshinstance 2'
end

function CM_Meshinstance2_tool:Version()
	return '1.0'
end

function CM_Meshinstance2_tool:UILabel()
	return 'Meshinstance 2'
end

function CM_Meshinstance2_tool:Creator()
	return 'Community test'
end

function CM_Meshinstance2_tool:Description()
	return 'Bind layer to another one to duplicate point motion of the points, having same position at frame 0. Copies any point animation in main timeline and all the actions.'
end

function CM_Meshinstance2_tool:ColorizeIcon()
	return true
end

-- **************************************************
-- Is Relevant / Is Enabled
-- **************************************************

function CM_Meshinstance2_tool:IsRelevant(moho)
	return true
end

function CM_Meshinstance2_tool:IsEnabled(moho)
	return true
end

-- **************************************************
-- Recurring Values
-- **************************************************

CM_Meshinstance2_tool.realtime_on = true

-- **************************************************
-- Prefs
-- **************************************************

function CM_Meshinstance2_tool:LoadPrefs(prefs)
	self.realtime_on = prefs:GetBool("CM_Meshinstance2_tool.realtime_on", true)
end

function CM_Meshinstance2_tool:SavePrefs(prefs)
	prefs:SetBool("CM_Meshinstance2_tool.realtime_on", self.realtime_on)
end

function CM_Meshinstance2_tool:ResetPrefs()
	self.realtime_on = true
end

-- **************************************************
-- Keyboard/Mouse Control
-- **************************************************

function CM_Meshinstance2_tool:OnMouseDown(moho, mouseEvent)
	
end

function CM_Meshinstance2_tool:OnKeyDown(moho, keyEvent)
	
end

-- **************************************************
-- Tool Panel Layout
-- **************************************************

CM_Meshinstance2_tool.REALTIME_BUTTON = MOHO.MSG_BASE
CM_Meshinstance2_tool.SET_MASTER_BTN = MOHO.MSG_BASE + 1
CM_Meshinstance2_tool.RESET_BINDING_BTN = MOHO.MSG_BASE + 2
CM_Meshinstance2_tool.UPDATE_LAYER_BTN = MOHO.MSG_BASE + 3
CM_Meshinstance2_tool.UPDATE_ALL_BTN = MOHO.MSG_BASE + 4
CM_Meshinstance2_tool.FIND_SLAVES_BTN = MOHO.MSG_BASE + 5

CM_Meshinstance2_tool.newMaster = nil
-- for version 1 (AE_Meshinstance) compatibility
CM_Meshinstance2_tool.srcKeyName = "AE_MeshinstSrc"
CM_Meshinstance2_tool.srcPathKeyName = "AE_MeshinstSrcPath"

function CM_Meshinstance2_tool:DoLayout(moho, layout)

	self:ToggleRealtime(moho, true)

	-- Toggle realtime (embed script) transformation on and off
	self.realtime_button = LM.GUI.ImageButton('ScriptResources/cm_meshinstance2/realtime.png', 'Realtime transform duplication', self.realtime_on, self.REALTIME_BUTTON, true)
	layout:AddChild(self.realtime_button, LM.GUI.ALIGN_LEFT, 0)

	-- Label to hold master layer name for slaves and to inform that a layer has slaves (for master layer selected)
	self.masterLabel = LM.GUI.DynamicText('Current master is                                           ', 0)
	layout:AddChild(self.masterLabel, LM.GUI.ALIGN_LEFT, 0)
	self.masterLabel:SetToolTip()

	-- Sets current layer's master to secondary-selected layer. If master selected -- press to select all the slaves of it
	self.setMasterButton = LM.GUI.Button('Set master to ...                ', self.SET_MASTER_BTN)
	layout:AddChild(self.setMasterButton, LM.GUI.ALIGN_LEFT, 0)
	-- On Alt-click -- select all the slaves, connected to this master
	self.setMasterButton:SetAlternateMessage(self.FIND_SLAVES_BTN)
	self.setMasterButton:SetToolTip('Secondary-select new master and press')

	--Maybe unnecessary (we need to test calling reset binding in update layer. If it takes too much time -- need to use this button and cash binding somewhere)
	--[[ Calls binding methods
	self.resetBindingButton = LM.GUI.Button('Reset binding', self.RESET_BINDING_BTN)
	layout:AddChild(self.resetBindingButton, LM.GUI.ALIGN_LEFT, 0)
	self.resetBindingButton:SetToolTip('Press after 0 frame changes')
	--]]

	-- Calls copy key methods for current layer
	self.updateLayerButton = LM.GUI.Button('Update layer', self.UPDATE_LAYER_BTN)
	layout:AddChild(self.updateLayerButton, LM.GUI.ALIGN_LEFT, 0)
	self.updateLayerButton:SetToolTip('Update current layer animation')

	-- Calls copy keys for all slave layers
	self.updateAllButton = LM.GUI.Button('Update all', self.UPDATE_ALL_BTN)
	layout:AddChild(self.updateAllButton, LM.GUI.ALIGN_LEFT, 0)
	self.updateAllButton:SetToolTip('Update all slave layers\' animation')
end

function CM_Meshinstance2_tool:UpdateWidgets(moho)
	CM_Meshinstance2_tool.realtime_button:SetValue(self.realtime_on)
	--check if current layer has a master and update self.masterLabel with its name
	--if layer has no master, disable update layer button, else enable it	
	local currentMaster = self:GetMaster(moho, moho.layer)
	if currentMaster then 
		self.masterLabel:SetValue('Current master is ' .. currentMaster:Name())
		self.updateLayerButton:Enable(true)
	else
		self.masterLabel:SetValue('')
		self.updateLayerButton:Enable(false)	
	end
	--check if document has secondary-selected layers, update self.newMaster and self.setMasterButton caption
	local newMaster = false
	for i=0, moho.document:CountSelectedLayers()-1 do 
		if moho.document:GetSelectedLayer(i) ~= moho.layer then 
			newMaster = moho.document:GetSelectedLayer(i)
			break
		end
	end
	if newMaster and not self:GetMaster(moho, newMaster) then
		self.newMaster = newMaster
		self.setMasterButton:SetLabel('Set master to ' .. newMaster:Name(), false)
	else
		self.newMaster = nil
		self.setMasterButton:SetLabel('Set master to ...', false)
	end
	
end

function CM_Meshinstance2_tool:HandleMessage(moho, view, msg)
	if msg == self.REALTIME_BUTTON then
		self:ToggleRealtime(moho) 
	elseif msg == self.SET_MASTER_BTN then
		self:SetMaster(moho, moho.layer, self.newMaster)
	elseif msg == self.FIND_SLAVES_BTN then	
		self:FindSlaves(moho, moho.layer)
	--elseif msg == self.RESET_BINDING_BTN then		
	elseif msg == self.UPDATE_LAYER then
		local master = self:GetMaster(moho, moho.layer)
		self:UpdateLayerAnimation(moho, moho.layer, master)
	elseif msg == self.UPDATE_ALL_BUTTON then
		self:UpdateAllLayersAnimation(moho)
	else
		
	end
end

function CM_Meshinstance2_tool:GetMaster(moho, layer)
	--read master layer UUID and try to find master layer with this UUID. If found, write its path as master path to slave data.
	local scriptInfo = layer:ScriptData()
	local currentSrcUUID = scriptInfo:GetString(self.srcKeyName)
	--if no UUID written, it means the layer is not slave and does not have any master. So return nil. 
	if not currentSrcUUID then return nil end
	for i, nextLayer in self:IterateAllLayers(moho) do
		if nextLayer:UUID() == currentSrcUUID then
			local path = self:GetLayerRelativePath(moho, layer, nextLayer)
			scriptInfo:Set(self.srcPathKeyName, path)
			return nextLayer
		end
	end
	-- if master is not found by UUID, try to find it by path. If found, write is UUID to slave data.
	local path = scriptInfo:GetString(self.srcPathKeyName)
	local pathLayer = self:GetLayerFromRelativePath(moho, layer, path)
	if pathLayer then
		scriptInfo:Set(self.srcKeyName, pathLayer:UUID())
		return pathLayer
	end
	--TODO: if layer not found -- do something (???) It means master existed (UUID was written to slave) but disapeared from the document.
	--TODO: user must be imformed about this.  
	return nil
end

function CM_Meshinstance2_tool:SetMaster(moho, layer, master)
	--TODO: first check if the layer is allways master (in this case you can not make it slave and do select all its slaves instead)
	--write new master UID and path to layer data (path writing needed to use imported characters course Moho changes layer UUIDs on import)
	local scriptInfo = layer:ScriptData()
	if not master then 
		scriptInfo:Remove(self.srcKeyName)
		scriptInfo:Remove(self.srcPathKeyName)
		return 
	end	
	scriptInfo:Set(self.srcKeyName, master:UUID())
	scriptInfo:Set(self.srcPathKeyName, self:GetLayerRelativePath(moho, layer, master))
	--TODO: call reset binding
	--TODO: call update layer animation
end

function CM_Meshinstance2_tool:FindSlaves(moho, layer, dontSelect)
	--TODO: iterate all layer in a document and check their masters
	--TODO: if slave found and dontSelect == false, select it
end

function CM_Meshinstance2_tool:UpdateLayerAnimation(moho, layer, master)
	--TODO: use map channels (from ResetBinding) and iterate all frames on main timeline and actions, seting master keys to slave and removing extra keys from slave
end

function CM_Meshinstance2_tool:UpdateAllLayersAnimation(moho)
	--TODO: iterate all document layers and call UpdateLayerAnimation for any slave
end

function CM_Meshinstance2_tool:ResetBinding(moho, layer, master)
	local pointMap = self:MapPoints(moho, layer, master)
	return self:MapChannels(moho, layer, master, pointMap)
end

function CM_Meshinstance2_tool:MapPoints(moho, layer, master)
	pointMap = {}
	--TODO: map points	
	return pointMap
end

function CM_Meshinstance2_tool:MapChannels(moho, layer, master, pointMap)
	--TODO: map channels
	--TODO: return channel maping
end

function CM_Meshinstance2_tool:ToggleRealtime(moho, defaultInit)
	local newValue = (not self.realtime_on)
	if defaultInit then newValue = false end
	--TODO: iterate all document layers, and for each slave add or remove embeded ScriptData
	self.realtime_on = newValue
end

-- some ulility methods from AE_Utilities scripts

function CM_Meshinstance2_tool:GetLayerRelativePath(moho, fromLayer, toLayer)
	if fromLayer == toLayer then return fromLayer:Name() end
	local path = ""
	local commonParent = fromLayer
	while commonParent and not self:IsAncestor(commonParent, toLayer) do
		commonParent = commonParent:Parent()
		path = path .. "../"
	end
	local secondPath = toLayer:Name()
	local nextParent = toLayer:Parent()
	while nextParent and commonParent ~= nextParent do
		secondPath = nextParent:Name() .. "/" .. secondPath
		nextParent = nextParent:Parent()
	end
	return (path .. secondPath)
end

function CM_Meshinstance2_tool:GetLayerFromRelativePath(moho, fromLayer, path)
	local foundLayer = fromLayer
	local restPath = path
	while string.sub(restPath, 1, 3) == "../" do
		if not foundLayer then return nil end
		foundLayer = foundLayer:Parent()
		restPath = string.sub(restPath, 4)
	end
	local nextSlash = string.find(restPath, "/")
	local nextName = string.sub(restPath, 1, (nextSlash and nextSlash - 1 or -1))
	if not foundLayer then 
		foundLayer = moho.document:LayerByName(nextName)
	else 
		foundLayer = moho:LayerAsGroup(foundLayer)
		if (foundLayer) then foundLayer = foundLayer:LayerByName(nextName) end
	end
	if not foundLayer then return nil end
	restPath = string.sub(restPath, #nextName + 2)	
	while #restPath > 0 do
		nextSlash = string.find(restPath, "/")
		nextName = string.sub(restPath, 1, (nextSlash and nextSlash - 1 or -1))
		foundLayer = moho:LayerAsGroup(foundLayer)
		if (foundLayer) then foundLayer = foundLayer:LayerByName(nextName) end
		if not foundLayer then return nil end
		restPath = string.sub(restPath, #nextName + 2)
	end
	return foundLayer
end

function CM_Meshinstance2_tool:IterateAllLayers(moho, from, to)
	local iterateLayersFn = function(condition, counter)
		counter = counter + 1
		if not condition or counter <= condition then
			local layer = moho.document:LayerByAbsoluteID(counter)
			if layer then
				return counter, layer
			end
		end
	end
	return iterateLayersFn, to, (from and from-1 or -1) 
end

function CM_Meshinstance2_tool:MyCoolFunction(moho, layer)
	-- Some cool code here
end

function CM_Meshinstance2_tool:IsAncestor(parentLayer, childLayer)
	self:MyCoolFunction()
	print("Hello world")
	while childLayer do 
		if childLayer == parentLayer then return true end	
		childLayer = childLayer:Parent()
	end
	return false
end

